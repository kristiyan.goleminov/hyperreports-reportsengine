using System.Collections.Generic;

namespace HyperReports.Data.Repositories
{
    public class ReportResult
    {
        public string AggregationType { get; set; }
        public Dictionary<string, decimal> Breakdown { get; } = new Dictionary<string, decimal>();
    }
}
