using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using HyperReports.Data.Enums;

namespace HyperReports.Data.Repositories
{
    public class ReportsRepository
    {
        private readonly string _connectionString;

        public ReportsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<ReportResult> GenerateReport(string company, int year, AggregationType aggregationType)
        {
            using(var connection = new SqlConnection(_connectionString))
            {
                string query;
                string periodFrom;
                string periodTo;
                Func<SqlDataReader, ReportResult> populateEntityFunc;

                switch (aggregationType)
                {
                    case AggregationType.Store:
                    case AggregationType.Receipt:
                    case AggregationType.Invoice:
                        throw new NotImplementedException();
                    case AggregationType.Payment:
                        query =
                            @"SELECT PaymentType,SUM(Quarter1) AS Quarter1,SUM(Quarter2) AS Quarter2,SUM(Quarter3) AS Quarter3,SUM(Quarter4) AS Quarter4
FROM
(
    SELECT PaymentType AS 'PaymentType', 
    Quarter1 = CASE(DATEPART(q, Receipts.IssueDate))
    WHEN 1 THEN SUM(Receipts.Total)
    ELSE 0
    END,
    Quarter2 = CASE(DATEPART(q, Receipts.IssueDate))
    WHEN 2 THEN SUM(Receipts.Total)
    ELSE 0
    END,
    Quarter3 = CASE(DATEPART(q, Receipts.IssueDate))
    WHEN 3 THEN SUM(Receipts.Total)
    ELSE 0
    END,
    Quarter4 = CASE(DATEPART(q, Receipts.IssueDate))
    WHEN 4 THEN SUM(Receipts.Total)
    ELSE 0
    END
 FROM Receipts
INNER JOIN Stores on Receipts.StoreId = Stores.Id
INNER JOIN Companies on Stores.CompanyId = Companies.Id
WHERE Companies.Name = @CompanyName
AND Receipts.IssueDate BETWEEN @PeriodFrom AND @PeriodTo
 GROUP BY Receipts.PaymentType, YEAR(Receipts.IssueDate), DATEPART(q, Receipts.IssueDate)
 )C
 GROUP BY PaymentType";
                        periodFrom = $"{year}-01-01";
                        periodTo = $"{year}-12-31";

                        populateEntityFunc = (reader) =>
                        {
                            var result = new ReportResult
                            {
                                AggregationType = (string) reader["PaymentType"]
                            };

                            result.Breakdown.Add("Quarter1", (decimal) reader["Quarter1"]);
                            result.Breakdown.Add("Quarter2", (decimal) reader["Quarter2"]);
                            result.Breakdown.Add("Quarter3", (decimal) reader["Quarter3"]);
                            result.Breakdown.Add("Quarter4", (decimal) reader["Quarter4"]);

                            return result;
                        };

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(aggregationType), aggregationType, null);
                }

                var command = new SqlCommand(query, connection);

                command.Parameters.AddWithValue("@CompanyName", company);
                command.Parameters.AddWithValue("@PeriodFrom", periodFrom);
                command.Parameters.AddWithValue("@PeriodTo", periodTo);

                var results = new List<ReportResult>();

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        results.Add(populateEntityFunc(reader));
                    }
                }

                return results;
            }
        }
    }
}
