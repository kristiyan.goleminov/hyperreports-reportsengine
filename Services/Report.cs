using System.Collections.Generic;

namespace HyperReports.Services
{
    public class Report
    {
        public string AggregationType { get; }
        public Dictionary<string, decimal> Breakdown { get; }
        public decimal Total { get; set; }

        public Report(string aggregationType, Dictionary<string, decimal> breakdown, decimal total)
        {
            AggregationType = aggregationType;
            Breakdown = breakdown;
            Total = total;
        }
    }
}
