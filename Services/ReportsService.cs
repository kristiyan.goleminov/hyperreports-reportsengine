using System;
using System.Collections.Generic;
using System.Linq;
using HyperReports.Data.Enums;
using HyperReports.Data.Repositories;

namespace HyperReports.Services
{
    public class ReportsService
    {
        private readonly ReportsRepository _reportsRepository;

        public ReportsService(string connectionString)
        {
            _reportsRepository = new ReportsRepository(connectionString);
        }

        public void GenerateReport(string company, int year, string aggregation)
        {
            AggregationType aggregationType; 
            if (!Enum.TryParse(aggregation, true, out aggregationType))
            {
                throw new ArgumentException("Invalid aggregation type");
            }

            var reports = _reportsRepository.GenerateReport(company, year, aggregationType);

            var reportsWithTotal = reports.Select(r => new Report(
                r.AggregationType,
                r.Breakdown, 
                r.Breakdown.Sum(x => x.Value)));
        }
    }
}